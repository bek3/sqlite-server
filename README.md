# SQLite Server

A simple cross-platform server that provides concurrent access to a SQLite database via a
REST API. Requires Mono or .NET.

**Note: This project is still in Beta: there are likely a few undiscovered issues and the API
is subject to change at any point.**

## Getting Started

### Building

Currently the project is set up as a project in JetBrains Rider. Use Rider to set up a 
build/run configuration to publish to a folder.

I am working on a build system that will be invoked using just a Makefile and will be able to
run in a build VM. This is not finished yet.

### Testing

Launch the application with the `test-config.yml` file in the `tests` directory. Use the set
of requests defined in `server-test.http`. It's not a comprehensive or perfect set of tests,
but it will quickly tell you if something awful is wrong.

### Configuration

SQLite server is configured by passing the path of a YAML file to the application asan argument.
The file should contain the following:
```yaml
databasepath: "./test-db.sqlite"
server:
  address: "127.0.0.1"
  port: 34567
```

## API Description

The HTTP server returns 501 (not implemented) for all methods other than GET (access server
performance information) and POST (interact with database). All payloads are JSON.

### GET

The server handles all GET requests exactly the same way. The following payload is returned,
any content carried by the initial request is ignored.

```json
{
  "db": <name of database object>,
  "request_count": <number of requests handled by server>,
  "post_count": <number of POST requests handled by server>,
  "get_count": <number of GET requests handled by server>,
  "err_count": <number of exceptions caught while handling a request>
}
```

### POST

The POST request is used to access the database, regardless of what type of statement is
being executed. Requests must be formatted accordingly:

```json
{
  "query": "CREATE TABLE test_table (testcol1 integer, testcol2 real);"
}
```

Currently only one query is allowed per request. The response will be formatted as follows:

```json
{
  "data": [[<col1 row1>, <col1 row2>], [<col2 row1>, <col2 row2>]],
  "success": true,
  "message": <Error message if not successful>
}
```

where the result is a two-dimensional list representing a table of query results. For
statements like INSERT and UPDATE, this will always just be empty.

## Client Libraries

The following client libraries exist:
* [Python](https://gitlab.com/bek3/sqlite-server-py-client)

There are currently plans to create libraries in Javascript and Dart. They have not been implemented yet.