/*
 * SQLite Server
 *
 * This code is provided under the BSD 2-clause license. See the LICENSE file for more details.
 *
 * Copyright (c) 2021, Brendan Kristiansen
 * All rights reserved.
 * 
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Text.Json;

namespace sqlite_server
{
    // https://gist.github.com/define-private-public/d05bc52dd0bed1c4699d49e2737e80e7
    class HTTPServer
    {
        public static HttpListener listener;

        private string addr { get; }
        private int port { get; }
        private string url;
        
        private SQLiteConnector cnx;

        private int reqCount;
        private int errCount;
        private int postCount;
        private int getCount;

        public HTTPServer(ServerConfig cfg, SQLiteConnector connection)
        {
            this.addr = cfg.Address;
            this.port = cfg.Port;
            this.url = $"http://{this.addr}:{this.port}/";

            this.cnx = connection;

            this.reqCount = 0;
            this.errCount = 0;
            this.postCount = 0;
            this.getCount = 0;
        }

        public void run()
        {
            // Create a Http server and start listening for incoming connections
            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", url);

            bool stopping = false;
            // Handle requests
            while (!stopping)
            {
                Task listenTask = HandleIncomingConnections();
                listenTask.GetAwaiter().GetResult();
            }
            // Close the listener
            listener.Close();
        }

        /**
         * Execute a command on the database. Return the result if any.
         */
        private void ExcecuteDBStatement(string stmt, Dictionary<string, object> requestOutput)
        {
            var res = this.cnx.execute(stmt);
            foreach (var key in res.Keys)
            {
                requestOutput[key] = res[key];
            }
        }

        /**
         * Handle an HTTP POST command
         */
        private void HandlePost(Dictionary<string, object> reqPayload,
                                Dictionary<string, object> requestOutput)
        {
            string query = reqPayload["query"].ToString();
            ExcecuteDBStatement(query, requestOutput);
        }

        private void HandleGet(Dictionary<string, object> requestOutput)
        {
            requestOutput.Add("db", this.cnx.ToString());
            requestOutput.Add("request_count", this.reqCount);
            requestOutput.Add("post_count", this.postCount);
            requestOutput.Add("get_count", this.getCount);
            requestOutput.Add("err_count", this.errCount);
            requestOutput.Add("version", Constants.APPVERSION);
        }

        private async void HandleRequests(HttpListenerRequest req, HttpListenerResponse resp)
        {
            string payload;
            using (Stream inputStream = req.InputStream)
            {
                using (StreamReader inStream = new StreamReader(inputStream, Encoding.UTF8))
                {
                    payload = inStream.ReadToEnd();
                }
            }
                
            Console.WriteLine("Payload: "+payload);

            resp.StatusCode = 200;
            Dictionary<string, object> outputPayload = new Dictionary<string, object>();
            switch (req.HttpMethod)
            {
                case "POST":
                    if (payload.Length > 0)
                    {
                        Dictionary<string, object> inputPayload =
                            JsonSerializer.Deserialize<Dictionary<string, object>>(payload);
                        this.HandlePost(inputPayload, outputPayload);
                    }

                    if (!outputPayload["success"].Equals(true))
                    {
                        resp.StatusCode = 500;
                    }
                    this.postCount++;
                    break;
                case "GET":
                    this.HandleGet(outputPayload);
                    this.getCount++;
                    break;
                default:
                    resp.StatusCode = 501;
                    break;
            }
            
            // Write the response info
            string jsonstr = JsonSerializer.Serialize(outputPayload);
            byte[] data = Encoding.UTF8.GetBytes(jsonstr);
            resp.ContentType = "Application/JSON";
            resp.ContentEncoding = Encoding.UTF8;
            resp.ContentLength64 = data.LongLength;

            if (resp.StatusCode != 200) this.errCount++;

                // Write out to the response stream (asynchronously), then close it
            await resp.OutputStream.WriteAsync(data, 0, data.Length);
            
        }

        public async Task HandleIncomingConnections()
        {
            while (true)
            {
                // Will wait here until we hear from a connection
                HttpListenerContext ctx = await listener.GetContextAsync();

                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;

                this.HandleRequests(req, resp);

                this.reqCount++;
                resp.Close();
            }
        }
    }
}