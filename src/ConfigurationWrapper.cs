/*
 * SQLite Server
 *
 * This code is provided under the BSD 2-clause license. See the LICENSE file for more details.
 *
 * Copyright (c) 2021, Brendan Kristiansen
 * All rights reserved.
 * 
 */

using System.IO;

using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace sqlite_server

{
    public class ServerConfig
    {
        public string Address { get; set; }
        public int Port { get; set; }
    }
    
    public class AppConfiguration
    {
        public string DatabasePath { get; set; }
        public ServerConfig Server { get; set; }
    }
    
    public class ConfigurationWrapper
    {
        private string Path { get; }
        private AppConfiguration Config { get; }

        public ConfigurationWrapper(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Configuration file does not exist at " + path);
            }

            this.Path = path;
            string filecontents = File.ReadAllText(this.Path);
            var deserializer = new DeserializerBuilder().WithNamingConvention(LowerCaseNamingConvention.Instance).Build();
            this.Config = deserializer.Deserialize<AppConfiguration>(filecontents);
        }

        public string GetDBPath()
        {
            return this.Config.DatabasePath;
        }

        public ServerConfig GetServerConfig()
        {
            return this.Config.Server;
        }
    }
}