/*
 * SQLite Server
 *
 * This code is provided under the BSD 2-clause license. See the LICENSE file for more details.
 *
 * Copyright (c) 2021, Brendan Kristiansen
 * All rights reserved.
 * 
 */

namespace sqlite_server
{
    public class Constants
    {
        public const string APPVERSION = "0.1.1-candidate";     // Application version string
    }
}