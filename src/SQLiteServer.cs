﻿/*
 * SQLite Server
 *
 * This code is provided under the BSD 2-clause license. See the LICENSE file for more details.
 *
 * Copyright (c) 2021, Brendan Kristiansen
 * All rights reserved.
 * 
 */

using System;

namespace sqlite_server
{
    class SQLiteServer
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing SQLite Server");
            Console.WriteLine($"Application version: {Constants.APPVERSION}");
            
            if (args.Length != 1)
            {
                throw new ArgumentException("Application must be invoked with a configuration path.");
            }
            
            String configPath = args[0];
            Console.WriteLine($"Loading configuration from {configPath}");
            ConfigurationWrapper cfg = new ConfigurationWrapper(configPath);

            SQLiteConnector cnx = new SQLiteConnector(cfg.GetDBPath());
            Console.WriteLine($"Registered connection to {cnx.ToString()}");
            
            HTTPServer srv = new HTTPServer(cfg.GetServerConfig(), cnx);
            Console.WriteLine($"Created server: {srv.ToString()}");
            
            srv.run();
        }
    }
}