/*
 * SQLite Server
 *
 * This code is provided under the BSD 2-clause license. See the LICENSE file for more details.
 *
 * Copyright (c) 2021, Brendan Kristiansen
 * All rights reserved.
 * 
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Numerics;
using System.Text.Json;

namespace sqlite_server
{
    public class SQLiteConnector
    {
        static readonly object dbLock = new object();
        
        private string path { get; }
        private SQLiteConnection cnx;

        public SQLiteConnector(string path)
        {
            this.path = path;
            var cnxStr = $"Data Source = {this.path}; Version = 3;";
            this.cnx = new SQLiteConnection(cnxStr);
        }

        public new string ToString()
        {
            return "<SQLite Database connection for " + this.path + ">";
        }

        public Dictionary<string, object> execute(string query)
        {
            var output = new Dictionary<string, object>();
            var dataset = new DataSet();

            bool success = true;
            string message = "";
            
            lock (dbLock)
            {
                this.cnx.Open();
                try
                {
                    var data = new SQLiteDataAdapter(query, this.cnx);
                    data.Fill(dataset);
                }
                catch (Exception e)
                {
                    success = false;
                    message = e.ToString();
                    Console.Error.WriteLine($"Error with database: {e.ToString()}");
                }
                this.cnx.Close();
            }
            
            var result = new List<List<dynamic>>();
            if (dataset.Tables.Count > 0)
            {
                for (int i = 0; i < dataset.Tables[0].Columns.Count; i++) result.Add(new List<dynamic>());
                for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                {
                    for (int j = 0; j < dataset.Tables[0].Rows[i].ItemArray.Length; j++)
                    {
                        result[j].Add(dataset.Tables[0].Rows[i].ItemArray[j]);
                    }
                }
            }
            output.Add("data", result);
            output.Add("success", success);
            output.Add("message", message);
            return output;
        }
        
    }
}